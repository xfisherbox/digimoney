package main

import "testing"


var DigiMoney = NewClient("http://a:b@localhost:14022/", 10);

// 0.00001 DGM minimum
func TestSetFee(t *testing.T) {

	resp, err := DigiMoney.SetFee(0.00001)
  if err != nil {
     t.Errorf("setFee error: %+v", err)
     t.FailNow()
  }
  t.Logf("setFee result: %v", resp)
}

func TestCreateAddress(t *testing.T) {

	resp, err := DigiMoney.CreateAddress()
	if err != nil {
		 t.Errorf("createAddress error: %+v", err)
		 t.FailNow()
	}
	t.Logf("createAddress result: %v", resp)
}

func TestGetBalance(t *testing.T) {

  resp, err := DigiMoney.GetBalance()
  if err != nil {
     t.Errorf("getBalance error: %+v", err)
     t.FailNow()
  }
  t.Logf("getBalance result: %v", resp)
}

func TestGetWalletInfo(t *testing.T) {
  resp, err := DigiMoney.GetWalletInfo()
  if err != nil {
     t.Errorf("getWalletInfo error: %+v", err)
     t.FailNow()
  }
  t.Logf("getWalletInfo result: %+v", resp)
}

func TestGetBalanceByAddress(t *testing.T) {
  resp, err := DigiMoney.GetBalanceByAddress("EFSSC6mVPoc3QwyWXKGmuo6eFEuZmWSYUK")
  if err != nil {
     t.Errorf("getBalanceByAddress error: %+v", err)
     t.FailNow()
  }
  t.Logf("getBalanceByAddress result: %v", resp)
}


func TestSendToAddress(t *testing.T) {

  addr := "E5AbbDc4LjAQ4GdNMWPca5kbi6Jf1ABDNh"
  resp, err := DigiMoney.SendToAddress(addr, 0.01)
  if err != nil {
     t.Errorf("sendToAddress error: %+v", err)
     t.FailNow()
  }
  t.Logf("sendToAddress result: %v", resp)
}



func TestGetTransaction(t *testing.T) {
  resp, err := DigiMoney.GetTransaction("b0bd3c118b383091f2ca83f07c4c519914bcf1738408b5f8891106564c7911df")
  if err != nil {
     t.Errorf("getTransaction error: %+v", err)
     t.FailNow()
  }
  t.Logf("getTransaction result: %v", resp)
}

func TestCheckTransaction(t *testing.T) {
  resp, err := DigiMoney.CheckTransaction("b0bd3c118b383091f2ca83f07c4c519914bcf1738408b5f8891106564c7911df")
  if err != nil {
     t.Errorf("getTransaction error: %+v", err)
     t.FailNow()
  }
  t.Logf("getTransaction result: %v", resp)
}
